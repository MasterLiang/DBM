<div align="center">
<h1> DBM (3.3.5a)</h1>
</div>

# 介绍

# 安装

**免责声明：如果您以前使用过DBM，并且希望保存旧的DBM配置文件，请备份您的WTF文件夹，因为我们将执行干净的安装。要继续清洁安装过程，请执行以下步骤:**

1. 打开你的插件目录 (Interface/Addons)，选择 `DBM` 文件 (每个 DBM- 开头的文件夹) 然后 **删除** 它们。
2. 打开你的账号配置目录 (WTF/Account/[账号]/SavedVariables)，选择 `DBM` 文件 (每个 DBM- 开头的文件) 然后 **删除** 它们。**这个删除你的 `DBM` 个人配置！**
3. 打开 **每个** 角色配置目录 (WTF/Account/[账号]/[服务器]/[角色]/SavedVariables)。选择 `DBM` 文件 (每个 DBM- 开头的文件) 然后 **删除** 它们。 **这个删除角色 `DBM` 配置！**

**没有旧 `DBM` 文件的残余，我们现在可以开始安装过程了。**

1. [下载](https://gitee.com/MasterLiang/DBM/releases/download/v1.0.0/DBM.zip)
2. 将 (DBM-Core，DBM-GUI，等) 解压缩或者复制到插件目录 (Interface/Addons)。注意插件目录下的 `DBM` 插件是 DBM- 开头才正确。
3. 将游戏客户端加载到角色选择屏幕中。在左下角，单击 AddOns 并启用所有DBM条目，如下所示：
![image](docs/images/addons_dbm_1.png)
![image](docs/images/addons_dbm_2.png)

# 启动

要打开选项窗口，请在聊天中键 `/dbm` ，然后按enter键或使用最小地图图标。有关更多命令，请键入 `/dbm help` 。

# 问题

遇到问题请游戏联系沙加（世界上叫沙加就可以）。

如果你有更好的 `DBM` 插件，欢迎交流

# 广告

怀念公会诚邀各路人士，开心游戏

# 链接

- 本插件是根据外网 `Zidras` 的 `DBM` 插件制作，[原地址](https://github.com/Zidras/DBM-Warmane)
